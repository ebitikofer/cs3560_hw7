/***************
Heath Scherich
4/8/15
These are the spaces on the
gameboard for a game of checkers
****************/

#ifndef SPACE_H
#define SPACE_H

class space{
public:
	space(){ filled = false;
		 king = false;
	}
	bool is_king()const{ return king; }
	bool is_empty()const{ return !filled; }
	bool is_white()const{ return white; }
	void fill(){filled = true;}
	
	void remove(){ 
		filled = false; 
		king = false;
	}
	void whiten(){ white = true;}
	void blacken(){ white = false; }
	void king_me(){ king = true; }
private:
	bool filled;
	bool white;
	bool king;
};

#endif
