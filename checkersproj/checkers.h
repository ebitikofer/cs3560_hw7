/***********************
Heath Scherich
4/8/15
Checkers class for the
checkers game
***********************/

#ifndef CHECKERS_H
#define CHECKERS_H

#include "game.h"
#include "space.h"
#include <iostream>

using namespace std;
using namespace main_savitch_14;

class checkers :public game{
public:
	checkers();
	~checkers() { };

	void display_status();
	void restart();
	bool is_legal(const string& move);
	void make_move(const string& move);
	bool is_game_over()const;
	game::who check_winner()const;
	bool look_for_jumps(const int& y, const int& x)const;
	bool check_jump(const string& move)const;

	game* clone()const {return new checkers(*this);}
	void compute_moves(queue<string>& moves)const;
	int evaluate()const;
	int find_close_jump(int y, int x)const;
private:
	space board[8][8];
	int numwhite, numblack;
	bool force_jump;
};

#endif