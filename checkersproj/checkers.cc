/***********************
Heath Scherich
4/8/15
This is a child class for
a game of checkers
**********************/


#include "checkers.h"
#include "colors.h"
#include <cmath>
#include <queue>
#include <string>
#include <sstream>
//#include <cstdlib>
using namespace std;
//using namespace main_savitch_14;

template<typename T>	//helpful function since to_str()
string NumToStr(T num){	//was not available
	ostringstream ss;
	ss << num;
	return ss.str();
}

checkers::checkers(){
	numblack = numwhite = 12;
	force_jump = false;
}

void checkers::restart(){
	cout<< "INITIALIZING BOARD\n";
	for (int y = 0; y < 8; ++y){		//rows
		for (int x = 0; x < 8; ++x){	//columns
			if ((x+y) % 2 == 1) cout << B_YELLOW << ' ';
			else cout << B_RED << ' ';
			board[y][x].remove();
		}
		cout << endl;
	}

	int y, x = 0;
	for (y = 0; y < 3; ++y){
		if (y % 2 == 1) x = 1;
		else x = 0;
		while(x<8){	//black pieces
			board[y][x].fill();
			board[y][x].blacken();
			x += 2;
		}
	}
	for (y = 5; y < 8; ++y){
		if (y % 2 == 1) x = 1;
		else x = 0;
		while(x<8){	//white pieces
			board[y][x].fill();
			board[y][x].whiten();
			x += 2;
		}
	}

	cout<< "BOARD INITIALIZED:"<< B_BLACK<< endl<< endl<<endl;
}

void checkers::display_status(){
	force_jump = false;
	cout<< endl;
	for (int y = 0; y < 8; ++y){
	cout<< WHITE<< 8-y<< ' ';
	for (int x = 0; x < 8; ++x){
		if ((x+y) % 2 == 1) cout << B_YELLOW <<" " << B_BLACK;
		else if (board[y][x].is_empty()) cout << B_RED << " " << B_BLACK;
		else{
			if ((x + y) % 2 == 1 && board[y][x].is_white()){
				cout << B_YELLOW << WHITE;
				if (last_mover() == COMPUTER)
					if (look_for_jumps(y, x)) force_jump = true;
			}
			else if (board[y][x].is_white()){
				cout << B_RED << WHITE;
				if (last_mover() == COMPUTER)
					if (look_for_jumps(y, x)) force_jump = true;
			}
			else if ((x + y) % 2 == 1){
				cout << B_YELLOW << BLACK;
				if (last_mover() == HUMAN)
					if (look_for_jumps(y, x)) force_jump = true;
			}
			else{
				cout << B_RED << BLACK;
				if (last_mover() == HUMAN)
					if (look_for_jumps(y, x)) force_jump = true;
			}
			if (!board[y][x].is_king()) cout << "O"<< B_BLACK;
			else cout << 'K' << B_BLACK;
		}

	}
	cout << endl;
	}
	cout<< "  ABCDEFGH"<< endl;
}

bool checkers::is_legal(const string& move){
	if (board[8 - (move[1] - '0')][toupper(move[0]) - 'A'].is_empty()) return false;

	if (last_mover() == COMPUTER && !board[8 - (move[1] - '0')][toupper(move[0]) - 'A'].is_white()){
		cout << "Please move your own piece!\n";
		return false;
	}
	else if (last_mover() == HUMAN && board[8 - (move[1] - '0')][toupper(move[0]) - 'A'].is_white()){
		cout << "Please move your own piece!\n";
		return false;
	}
	
	if (!board[8 - (move[3] - '0')][toupper(move[2]) - 'A'].is_empty() &&
		!(board[8 - (move[1] - '0')][toupper(move[0]) - 'A'].is_white() == board[8 - (move[3] - '0')][toupper(move[2]) - 'A'].is_white()) &&
		(8 - (move[3] - '0') != 0 && 8 - (move[3] - '0') != 7 && toupper(move[2]) - 'A' != 0 && toupper(move[2]) - 'A' != 7)){
		return check_jump(move);				//make sure not the same color
	}											//else if they are..
	else if (!board[8 - (move[3] - '0')][toupper(move[2]) - 'A'].is_empty() &&
		board[8 - (move[1] - '0')][toupper(move[0]) - 'A'].is_white() == board[8 - (move[3] - '0')][toupper(move[2]) - 'A'].is_white() &&
		(8 - (move[3] - '0') != 0 && 8 - (move[3] - '0') != 7 && toupper(move[2]) - 'A' != 0 && toupper(move[2]) - 'A' != 7)){
		return false;
	}
	
	if (board[8 - (move[1] - '0')][toupper(move[0]) - 'A'].is_king()){						//checks for special case
		if (toupper(move[2]) == toupper(move[0]) + 1 || toupper(move[2]) == toupper(move[0]) - 1){	//piece is king
			if ((move[3] == move[1] + 1 || move[3] == move[1] - 1)){
				if (force_jump){
					cout << "Please take your jump!\n";
					return false;
				}
				return true;
			}
		}
	}
	
	if (toupper(move[2]) == toupper(move[0]) + 1 || toupper(move[2]) == toupper(move[0]) - 1){	//first to check if moving into a valid column
		if (board[8 - (move[1] - '0')][toupper(move[0]) - 'A'].is_white() && move[3] == move[1] + 1){
			if (force_jump){
				cout << "Please take your jump!\n";
				return false;
			}
			return true;
		}
		else if (!board[8 - (move[1] - '0')][toupper(move[0]) - 'A'].is_white() && move[3] == move[1] - 1){
			if (force_jump){
				cout << "Please take your jump!\n";
				return false;
			}
			return true;
		}
	}

	return false;
}

void checkers::make_move(const string& move){
	space* newspace = &board[8 - (move[3] - '0')][toupper(move[2]) - 'A'];
	int delvert = move[1] - move[3]; //negative if moving up
	int delhoriz = move[2] - move[0]; //posivitive if right

	if (!board[8 - (move[3] - '0')][toupper(move[2]) - 'A'].is_empty()){	//if we need to jump
		
		board[8 - (move[3] - '0')][toupper(move[2]) - 'A'].remove();
		if (board[8 - (move[3] - '0')][toupper(move[2]) - 'A'].is_white()) --numwhite;
		else --numblack;

		//if we jump, new space will be 2x and 2y away (depending on color)
		newspace = &board[8 - (move[1] - '0') + 2 * delvert][toupper(move[0]) - 'A' + 2 * delhoriz];
		force_jump = false;
		if (((8 - (move[1] - '0') + 2 * delvert == 0 && board[8-(move[1]-'0')][toupper(move[0])-'A'].is_white())|| 
			(8 - (move[1] - '0') + 2 * delvert == 7 && !board[8 - (move[1] - '0')][toupper(move[0]) - 'A'].is_white())) &&
				!newspace->is_king()){
			newspace->king_me();
			board[8 - (move[1] - '0')][toupper(move[0]) - 'A'].remove();
			newspace->fill();
			if (board[8 - (move[1] - '0')][toupper(move[0]) - 'A'].is_white())
				newspace->whiten();
			else newspace->blacken();
			game::make_move(move);
			return;		//move ends when piece is kinged
		}
	}	

	if (board[8 - (move[1] - '0')][toupper(move[0]) - 'A'].is_white()){
		newspace->whiten(); 
		if (8 - (move[3] - '0') == 0)	//white pieces are kinged in 8th row
			newspace->king_me();
	} else {
		newspace->blacken();
		if (8 - (move[3] - '0') == 7)	//black pieces are kinged in 1st row
			newspace->king_me();
	}

	if (board[8 - (move[1] - '0')][toupper(move[0]) - 'A'].is_king())
		newspace->king_me();

	board[8 - (move[1] - '0')][toupper(move[0]) - 'A'].remove();
	newspace->fill();
	
	if (newspace == &board[8 - (move[3] - '0')][toupper(move[2]) - 'A'] ||
		!look_for_jumps(8 - (move[1] - '0') + 2 * delvert, toupper(move[0]) - 'A' + 2 * delhoriz))
	game::make_move(move);			//this is a special case where a person has a
}									//double jump and needs to jump again

bool checkers::is_game_over()const{
	if (numwhite == 0 || numblack == 0) return true;
	return false;
}

//checks if moving off the board, if moving the write way, and if 
//the space we're jumping into is empty
bool checkers::check_jump(const string& move)const{
	int delvert = move[1] - move[3]; //negative if moving up
	int delhoriz = toupper(move[2]) - toupper(move[0]); //posivitive if right
	if (abs(delvert) != 1 || abs(delhoriz) != 1) return false;
	if (board[8 - (move[1] - '0')][toupper(move[0]) - 'A'].is_white() == board[8 - (move[3] - '0')][toupper(move[2]) - 'A'].is_white())
		return false;

	if (8 - (move[1] - '0') + 2 * delvert < 0 || 8 - (move[1] - '0') + 2 * delvert > 7)	return false;
	if (toupper(move[0]) - 'A' + 2 * delhoriz < 0 || toupper(move[0]) - 'A' + 2 * delhoriz >7) return false;
		//check for jumping off board;

	if (board[8 - (move[1] - '0')][toupper(move[0]) - 'A'].is_king()){
		return(board[8 - (move[1] - '0') + 2 * delvert][toupper(move[0]) - 'A' + 2 * delhoriz].is_empty());
	}		//checks if a jump can be made

	if (board[8 - (move[1] - '0')][toupper(move[0]) - 'A'].is_white()	//white pieces only jump one way
		&& delvert == -1){
		return board[8 - (move[1] - '0') + 2 * delvert][toupper(move[0]) - 'A' + 2 * delhoriz].is_empty();
	}

	if (!board[8 - (move[1] - '0')][toupper(move[0]) - 'A'].is_white()	//black pieces only jump one way
		&& delvert == 1){
		return board[8 - (move[1] - '0') + 2 * delvert][toupper(move[0]) - 'A' + 2 * delhoriz].is_empty();
	}

	return false;
}

bool checkers::look_for_jumps(const int& y, const int& x)const{
	//returns true if there is a jump to be made
	//y is same as first bracket =				x is same as second bracket =
	//		board[8 - (move[1] - '0')				toupper(move[0]) - 'A'
	if (board[y][x].is_king()){
 		for (int i = -1; i < 2; i += 2){//four spots to check for
			for (int j = -1; j < 2; j += 2){//a possible jump
				if (!board[y + i][x + j].is_empty() && board[y + 2*i][x + 2*j].is_empty() &&//space your moving to isnt empty
					!(board[y][x].is_white() == board[y+i][x+j].is_white()) &&	//not the same color
					(y+i != 7 && y+i !=0 && x+j != 0 && x+j != 7))	//not going to move off the board
					return true;
			}
		}
	}

	if (board[y][x].is_white()){
		for (int i = -1; i < 2; i += 2){
			if (!board[y - 1][x + i].is_empty() && board[y - 2][x + 2*i].is_empty() &&
				!(board[y][x].is_white() == board[y - 1][x + i].is_white()) && 
				(y-1 !=0 && x+i != 0 && x+i != 7))
				return true;
		}
	} else {//black pieces move down
		for (int i = -1; i < 2; i += 2){
			if (!board[y + 1][x + i].is_empty() && board[y + 2][x + 2*i].is_empty() &&
				!(board[y][x].is_white() == board[y + 1][x + i].is_white()) && 
				(y+1 != 7 && x+i != 0 && x+i != 7))
				return true;
		}
	}

	return false;
}

game::who checkers::check_winner()const{
	if (numwhite == 0) return HUMAN;
	else return COMPUTER;
}

void checkers::compute_moves(queue<string>& moves)const{
	string best_index;
//	space test_board[8][8];
//	string moves_away;	//[0] is # moves away for comparison, [1]&[2] are the actual move

/*	for (int i = 0; i < 8; ++i){	//creating a copy of current board
		for (int j = 0; j < 8; ++j){
			if (!board[i][j].is_empty()){
				test_board[i][j].fill();
				if (board[i][j].is_king()) test_board[i][j].king_me();

				if (board[i][j].is_white()) test_board[i][j].whiten();
				else test_board[i][j].blacken();
			}
		}
	}
*/
	for (int y = 0; y < 8; ++y){	//this will compute frontmost pieces first
	for (int x = 0; x < 8; ++x){
	if (last_mover() == HUMAN){	//if its computer's turn				
		if (!board[y][x].is_empty() && !board[y][x].is_white()){
			if (y+1 <8){
				if (board[y + 1][x + 1].is_empty() && x + 1 < 8)
					moves.push((char)('A' + x) + NumToStr(8 - y) + (char)('A' + x + 1) + NumToStr(8 - y - 1));
				if (board[y + 1][x - 1].is_empty() && x - 1 >= 0)
					moves.push((char)('A' + x) + NumToStr(8 - y) + (char)('A' + x - 1) + NumToStr(8 - y - 1));
			}
			if ((y + 1 < 8 && x + 1 < 8 && !board[y + 1][x + 1].is_empty()) &&
				!(board[y+1][x+1].is_white() == board[y][x].is_white())){
				if (check_jump((char)('A'+x) + NumToStr(8-y) + //stringifying the move we're checking for
					(char)('A' + x + 1) + NumToStr(8-y - 1))){		//force jump
					while (!moves.empty())
						moves.pop();
					moves.push((char)('A' + x) + NumToStr(8-y) + (char)('A' + x+1) + NumToStr(8-y-1));
					return;
				}
			}
			else if ((y + 1 < 8 && x - 1 >=0 && !board[y + 1][x - 1].is_empty()) &&
				!(board[y + 1][x - 1].is_white() == board[y][x].is_white())){
				if (check_jump((char)('A' + x) + NumToStr(8-y) + //stringifying the move we're checking for
					(char)('A' + x- 1) + NumToStr(8-y- 1))){//force jump
					while (!moves.empty())
						moves.pop();
					moves.push((char)('A' + x) +NumToStr(8-y) + (char)('A' + x-1) + NumToStr(8-y-1));
					return;
				}
			}
		}
	}
	else {	//if its the players pseudoturn
		if (!board[y][x].is_empty() && board[y][x].is_white()){
			if (y-1 >=0){
				//look for closest jump, make sure its my jump
				//moves_away = find_close_jump(test_board, y, x);
				//if (moves_away[0] == best_index[0] && rand() % 2 == 0) best_index = moves_away;
				//if (moves_away[0] < best_index[0]) best_index = moves_away; FAILED TRIALS OF DOING MORE WORK THAN I NEEDED TO
				if (board[y - 1][x + 1].is_empty() && x + 1 <8)
					moves.push((char)('A' + x) + NumToStr(8 - y) + (char)('A' + x + 1) + NumToStr(8 - y + 1));
				if (board[y - 1][x - 1].is_empty() && x - 1 >= 0)
					moves.push((char)('A' + x) + NumToStr(8 - y) + (char)('A' + x - 1) + NumToStr(8 - y + 1));
			}
			if (!board[y - 1][x + 1].is_empty() && y - 1 >= 0 && x + 1 < 8 &&
				!(board[y - 1][x + 1].is_white() == board[y][x].is_white())){
				if (check_jump((char)('A' + x) + NumToStr(8-y) + //stringifying the move we're checking for
					(char)('A' + x+1) + NumToStr(8-y+1))){		//force jump
					while (!moves.empty())
						moves.pop();
					moves.push((char)('A' + x) + NumToStr(8-y) + (char)('A' + x+ 1) + NumToStr(8-y+ 1));
					return;
				}
			}
			else if (!board[y - 1][x - 1].is_empty() && y - 1 >= 0 && x - 1 > 0 &&
				!(board[y - 1][x - 1].is_white() == board[y][x].is_white())){
				if (check_jump((char)('A' + x) + NumToStr(8-y) + //stringifying the move we're checking for
					(char)('A' + x - 1) + NumToStr(8-y +1))){		//force jump
					while (!moves.empty())
						moves.pop();
					moves.push((char)('A' + x) + NumToStr(8-y) + (char)('A' + x - 1) + NumToStr(8-y + 1));
					return;
				}
			}
		}
	}
	}
	}
}

int checkers::evaluate()const{
	int points = 0;
	for (int y = 0; y < 8; ++y){
	for (int x = 0; x < 8; ++x){
		if (!board[y][x].is_empty()){
			if (!board[y][x].is_white()){			//it ended up being useful
				points += 5;	//simply for having pieces
				if (!board[y][x].is_king())
					points += y*y/3;//more points for being close to kinged
				else
					points += 10;

				if (look_for_jumps(y, x))
					points += 50;
				else
					points += (10 - find_close_jump(y, x)) * 2;//accidentally wrote this function
				if (x == 0 || x == 7)	//points for having unjumpable pieces
					points += 5;
			}
			else{//subtracting anything good for the player
				points -= 5;
				if (!board[y][x].is_king())
					points -= (8-y) *(8-y);
				else
					points -= 10;

				if (look_for_jumps(y, x))
					points -= 50;
				else
					points -= (10 - find_close_jump(y, x)) * 2;
				if (x == 0 || x == 7)
					points -= 5;
			}
		}
	}
	}
	return points;
}

//returns string of [0] = moves away [1] = current y value, [2] = current x
//									[3] = new y      [4] = new x value
int checkers::find_close_jump(int y, int x)const{//at least i can use this for my evaluate function.....
	if (!board[y][x].is_king()){
		if (!board[y][x].is_white()){
			for (int i = y+1; i < 8; ++i){//starts search in row after the piece's trying to find nearest jump
				for (int j = 0; j < 8; ++j){
					if (!board[i][j].is_empty() && !(board[i][j].is_white() == board[y][x].is_white())){
						if (abs(x - j) <= i - y && (i - y) % 2 == 1){//if it can reach it in the possible diaganol moves and isnt gonna get jumped upon getting there
							return i - y;
						}
					}			
				}
			}
		}
		else{	//looking for white piece's closest jump
			for (int i = y-1; i >= 0; --i){
				for (int j = 0; j <8; ++j){
					if (!board[i][j].is_empty() && !(board[i][j].is_white() == board[y][x].is_white())){
						if (abs(x - j) <= y - i && (y - i) % 2 == 1){
							return y - i;
						}
					}
				}
			}
		}
	}
	return 999; //returns a value larger than if any jump
}						//had been available
