/************************
Heath scherich
4/7/15
This is the driver for a checkers game
*************************/

#include <iostream>
#include "checkers.h"

using namespace std;
using namespace main_savitch_14;

int main(){
	checkers game;
	game::who winner;
	winner = game.play();
	if (winner == 0)
		cout << "You beat the computer! High five!\n";
	else
		cout << "You got beat by the computer! Maybe next time.\n";
}